package com.score.lokka.util

import com.score.lokka.cassandra.{Block, CassandraStore, Cert, Trans}
import com.score.lokka.config.AppConf

import scala.annotation.tailrec

object BlockFactory extends AppConf {

  def merkleRoot(trans: List[Trans]): String = {
    @tailrec
    def merkle(ins: List[String], outs: List[String]): String = {
      ins match {
        case Nil =>
          // empty list
          if (outs.size == 1) outs.head
          else merkle(outs, List())
        case x :: Nil =>
          // one element list
          merkle(Nil, outs :+ CryptoFactory.sha256(x + x))
        case x :: y :: l =>
          // have at least two elements in list
          // concat them and sign them
          merkle(l, outs :+ CryptoFactory.sha256(x + y))
      }
    }

    merkle(trans.map(t => CryptoFactory.sha256(t.id.toString)), List())
  }

  def validateBlock(block: Block): String = {
    @tailrec
    def verify(ins: List[String], outs: List[String]): String = {
      ins match {
        case Nil =>
          // empty list
          if (outs.size == 1) outs.head
          else verify(outs, List())
        case x :: Nil =>
          // one element list
          CryptoFactory.verifySignature(x, x)
          verify(Nil, outs :+ CryptoFactory.sha256(x + x))
        case x :: y :: l =>
          // have at least two elements in list
          // concat them and sign them
          verify(l, outs :+ CryptoFactory.sha256(x + y))
      }
    }

    verify(block.trans.map(t => CryptoFactory.sha256(t.id.toString)), List())
  }

  def validateTrans(trans: List[Trans]): Boolean = {
    @tailrec
    def verify(ins: List[Trans]): Boolean = {
      ins match {
        case Nil =>
          // empty list means all trans verified
          true
        case x :: _ =>
          // at least one element in the list
          // get public key of aplos and verify signature
          CassandraStore.getCert(x.aplos) match {
            case Some(Cert(_, payload, _, _)) =>
              val pubKey = CryptoFactory.loadRSAPublicKey(payload)
              if (CryptoFactory.verifySignature(x.message, x.digsig, pubKey)) verify(ins.drop(0)) else false
            case _ =>
              false
          }
      }
    }

    verify(trans)
  }

  def hash(timestamp: String, markleRoot: String, preHash: String): String = {
    val p = timestamp + markleRoot + preHash
    CryptoFactory.sha256(p)
  }

  def block(trans: List[Trans]): Block = {
    Block(
      lokka = serviceName,
      merkelRoot = merkleRoot(trans),
      preHash = "",
      hash = "",
      trans = trans
    )
  }

}
