package com.score.lokka.stream

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, GraphDSL, Partition, RunnableGraph, Sink, Source}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, ClosedShape, Supervision}
import com.score.lokka.cassandra.{Block, CassandraCluster, CassandraStore, Trans}
import com.score.lokka.config.{AppConf, KafkaConf}
import com.score.lokka.redis.RedisStore
import com.score.lokka.util.{AppLogger, BlockFactory, FmlFactory}

import scala.collection.mutable
import scala.concurrent.Future
import scala.concurrent.duration._
import akka.NotUsed
import akka.actor.ActorSystem
import akka.kafka.ConsumerMessage.CommittableMessage
import akka.kafka.scaladsl.{Consumer, Producer}
import akka.kafka.{ConsumerSettings, ProducerSettings, Subscriptions}
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Partition, RunnableGraph, Source}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, ClosedShape, Supervision}
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import spray.json._

object Streamer extends AppLogger with CassandraCluster with AppConf with KafkaConf {

  val MODEL_ACCURACY_THRESHOLD = 75

  def init()(implicit system: ActorSystem): Unit = {
    // supervision
    // meterializer for streams
    val decider: Supervision.Decider = { e =>
      logError(e)
      Supervision.Resume
    }
    implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))
    import system.dispatcher

    // lokka run for given period
    val source = Source.tick(0.minute, blockCreateInterval.minute, NotUsed)

    // flow to get trans ids from redis
    val getTransIdsFlow: Flow[NotUsed, mutable.Set[String], NotUsed] = Flow[NotUsed]
      .map(_ => RedisStore.get)

    // flow to get trans from cassandra
    val getTransFlow: Flow[mutable.Set[String], (List[String], List[Option[Trans]]), NotUsed] = Flow[mutable.Set[String]]
      .mapAsync(2)(p => Future(CassandraStore.getTrans(p.toList)))

    // flow to remove redis ids
    val removeTransIdsFlow = Flow[(List[String], List[Option[Trans]])]
      .map { p =>
        RedisStore.rm(p._1)
        p._2
      }

    // flow to build block obj
    val toBlockFlow = Flow[List[Option[Trans]]]
      .map(p => BlockFactory.block(p.flatten))

    // flow validate flow
    val validateFlow = Flow[Block]
      .map(p => BlockFactory.validateBlock(p))

    // flow to save block
    val saveBlockFlow = Flow[Block]
      .mapAsync(parallelism = 2)(p => CassandraStore.createBlock(p))

    // TODO sink to publish kafka
    val sink = Sink.foreach[Block](p => s"created block $p")

    // graph
    val graph = RunnableGraph.fromGraph(GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._

      // partition stream
      //  1. with trans
      //  2. without trans
      val partition = b.add(Partition[(List[String], List[Option[Trans]])](2, r => if (r._2.flatten.isEmpty) 0 else 1))

      // source directs to partition
      source ~> getTransIdsFlow ~> getTransFlow ~> partition.in

      // flow stream in two flows
      //  1. with trans
      //  2. without trans
      partition.out(0) ~> Sink.ignore
      partition.out(1) ~> removeTransIdsFlow ~> toBlockFlow ~> saveBlockFlow ~> sink

      ClosedShape
    })
    graph.run()
  }

  def stream()(implicit system: ActorSystem): Unit = {
    // supervision
    // meterializer for streams
    val decider: Supervision.Decider = { e =>
      logError(e)
      Supervision.Resume
    }
    implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))
    import system.dispatcher

    // parse json
    import spray.json.DefaultJsonProtocol._
    implicit val transFormat: JsonFormat[Trans] = jsonFormat7(Trans)

    // kafka consumer source
    val consumerSettings = ConsumerSettings(system, new StringDeserializer, new StringDeserializer)
      .withBootstrapServers(kafkaAddr)
      .withGroupId(kafkaGroup)
    val source = Consumer.committableSource(consumerSettings, Subscriptions.topics(kafkaTopic))

    // kafka producer sink
    val producerSettings = ProducerSettings(system, new StringSerializer, new StringSerializer)
      .withBootstrapServers(kafkaAddr)
    val sink = Producer.plainSink(producerSettings)

    // flow to publish packets to storage
    val toStorageProducerRecordFlow = Flow[(Trans, Int)]
      .map(p => new ProducerRecord[String, String](kafkaStorageTopic, p._1.toJson.toString))

    // flow to publish invalid packets to notifier, map message to ProducerRecord
    val toNotificationProducerRecordFlow = Flow[(Trans, Int)]
      .map(p => new ProducerRecord[String, String](kafkaNotifierTopic, p._1.toJson.toString))

    // flow to map kafka message to Trans
    // trans contains the ml model
    val toTransFlow: Flow[CommittableMessage[String, String], Trans, NotUsed] = Flow[CommittableMessage[String, String]]
      .map { p =>
        logger.debug(s"Got message ${p.record.value()}")
        p.record.value().parseJson.convertTo[Trans]
      }

    // flow evaluate model accuracy
    // we run 100 parallel actors
    val evaluateModelAccuracyFlow = Flow[Trans]
      .mapAsync(100)(p => Future(FmlFactory.evaluateAccuracy(p)))

    // graph
    val graph = RunnableGraph.fromGraph(GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._

      // partition stream
      //  1. with trans
      //  2. without trans
      val partition = b.add(Partition[(Trans, Int)](2, r => if (r._2 < MODEL_ACCURACY_THRESHOLD) 0 else 1))

      // source directs to partition
      source ~> toTransFlow ~> evaluateModelAccuracyFlow ~> partition.in

      // flow stream in two flows
      //  1. in accurate models to notifier
      //  2. accurate models to storage
      partition.out(0) ~> toNotificationProducerRecordFlow ~> sink
      partition.out(1) ~> toStorageProducerRecordFlow ~> sink

      ClosedShape
    })
    graph.run()
  }

}
