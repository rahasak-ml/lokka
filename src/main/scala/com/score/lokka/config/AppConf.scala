package com.score.lokka.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

/**
  * Load configurations define in application.conf from here
  *
  * @author eranga herath(erangaeb@gmail.com)
  */
trait AppConf {

  // config object
  val appConf = ConfigFactory.load()

  // lokka config
  lazy val serviceMode = Try(appConf.getString("service.mode")).getOrElse("DEV")
  lazy val serviceName = Try(appConf.getString("service.name")).getOrElse("lokka")
  lazy val blockCreateInterval= Try(appConf.getInt("service.block-create-interval")).getOrElse(1)

  // keys config
  lazy val keysDir = Try(appConf.getString("keys.dir")).getOrElse(".keys")
  lazy val publicKeyLocation = Try(appConf.getString("keys.public-key-location")).getOrElse(".keys/id_rsa.pub")
  lazy val privateKeyLocation = Try(appConf.getString("keys.private-key-location")).getOrElse(".keys/id_rsa")

}
