package com.score.lokka.cassandra

import java.util.concurrent.Executors
import java.util.{Date, UUID}

import com.dataoperandz.cassper.Cassper
import com.datastax.driver.core.ConsistencyLevel
import com.datastax.driver.core.querybuilder.QueryBuilder
import com.datastax.driver.core.utils.UUIDs
import com.score.lokka.config.CassandraConf
import com.score.lokka.util.AppLogger

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

case class Cert(id: String, payload: String, digsig: String, timestamp: Date = new Date)

case class Trans(id: String, aplos: String, execer: String, actor: String, messageTyp: String, message: String, digsig: String)

case class Block(id: UUID = UUIDs.timeBased(), lokka: String, trans: List[Trans], merkelRoot: String, preHash: String,
                 hash: String, timestamp: Date = new Date())

object CassandraStore extends CassandraCluster with CassandraConf with AppLogger {

  lazy val gtps = session.prepare("SELECT * FROM mystiko.trans WHERE execer = ? AND actor = ? AND id = ?")
    .setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM)
  lazy val cbps = session.prepare("INSERT INTO mystiko.blocks(lokka, id, trans, merkle_root, pre_hash, hash, timestamp) VALUES(?, ?, ?, ?, ?, ?, ?)")
    .setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM)

  // cert
  lazy val ccps = session.prepare("INSERT INTO mystiko.certs(id, payload, digsig, timestamp) VALUES(?, ?, ?, ?)")
    .setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM)
  lazy val gcps = session.prepare("SELECT * FROM mystiko.certs where id = ? LIMIT 1")
    .setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM)

  def init() = {
    // create keyspace
    session.execute(cassandraKeyspaceSchema)

    // migration via cassper
    Cassper.build(cassandraKeyspace, session)
      .migrate(cassandraKeyspace)
  }

  def getTrans(ids: List[String]): (List[String], List[Option[Trans]]) = {
    def get(id: String): Future[Option[Trans]] = {
      Future {
        val attr = id.split(";")
        val row = session.execute(gtps.bind(attr(0), attr(1), attr(2))).one()
        if (row != null) {
          Option(Trans(
            row.getString("aplos"),
            row.getString("execer"),
            row.getString("id"),
            row.getString("actor"),
            row.getString("message_typ"),
            row.getString("message"),
            row.getString("digsig"),
            row.getTimestamp("timestamp"))
          )
        } else {
          None
        }
      }
    }

    // get trans from 10 futures
    implicit val ec = ExecutionContext.fromExecutorService(Executors.newWorkStealingPool(10))
    val f = Future.sequence(ids.map(p => get(p)))
    Await.result(f, 30.seconds) match {
      case l: List[Option[Trans]] =>
        logger.info(s"Success get trans, $l")
        (ids, l)
      case _ =>
        logger.error(s"Fail get trans, timeout")
        (ids, List())
    }
  }

  def createBlock(block: Block) = {
    Future {
      // trans UDT
      val transType = cluster.getMetadata.getKeyspace(cassandraKeyspace).getUserType("trans")
      val trans = block.trans.map(t =>
        transType.newValue
          .setString("aplos", t.execer)
          .setString("execer", t.execer)
          .setString("id", t.id)
          .setString("actor", t.actor)
          .setString("message_typ", t.messageTyp)
          .setString("message", t.message)
          .setString("digsig", t.digsig)
          .setTimestamp("timestamp", t.timestamp)
      ).asJava

      // insert query
      val statement = QueryBuilder.insertInto(cassandraKeyspace, "blocks")
        .value("lokka", block.lokka)
        .value("id", block.id)
        .value("trans", trans)
        .value("merkle_root", block.merkelRoot)
        .value("pre_hash", block.preHash)
        .value("hash", block.hash)
        .value("timestamp", block.timestamp)

      session.execute(statement)
      block
    }
  }

  def createBlockSync(block: Block) = {
    // trans UDT
    val transType = cluster.getMetadata.getKeyspace(cassandraKeyspace).getUserType("trans")
    val trans = block.trans.map(t =>
      transType.newValue
        .setString("aplos", t.execer)
        .setString("execer", t.execer)
        .setString("id", t.id)
        .setString("actor", t.actor)
        .setString("message_typ", t.messageTyp)
        .setString("message", t.message)
        .setString("digsig", t.digsig)
        .setTimestamp("timestamp", t.timestamp)
    ).asJava

    // insert query
    val statement = QueryBuilder.insertInto(cassandraKeyspace, "blocks")
      .value("lokka", block.lokka)
      .value("id", block.id)
      .value("trans", trans)
      .value("merkle_root", block.merkelRoot)
      .value("pre_hash", block.preHash)
      .value("hash", block.hash)
      .value("timestamp", block.timestamp)

    session.execute(statement)
    block
  }

  def getCert(id: String): Option[Cert] = {
    val row = session.execute(gcps.bind(id)).one()
    if (row != null) Option(Cert(row.getString("id"), row.getString("payload"), row.getString("digsig"),
      row.getTimestamp("timestamp")))
    else None
  }
}

//object M extends App {
//  val t1 = System.currentTimeMillis()
//  CassandraStore.init()
//  val rs = RedisStore.get
//  val ts = CassandraStore.getTrans(rs.toList)
//  //val t = CassandraStore.getTrans(List("erangaeb@gmail.com;DocumentActor;11111011oo11111"))
//  val b = BlockFactory.block(ts._2.flatten)
//  CassandraStore.createBlockSync(b)
//  val t2 = System.currentTimeMillis()
//
//  println(s"$t1:$t2:${t2 - t1}")
//}
